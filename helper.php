<?php

class ModHelloWorldFelderHelper {
    public static function getHello($params) {
        $db = JFactory::getDbo();
        $query = $db -> getQuery(true)
                     -> select($db -> quoteName('hello'))
                     -> from($db -> quoteName('#__helloworld'))
                     -> where('id = ' . $db -> Quote($params));

        $db -> setQuery($query);

        $result = $db -> loadResult();
        return $result;
    }
}
