<?php

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

$language = $params -> get('lang', '1');
$hello    = modHelloWorldFelderHelper::getHello($language);
require JModuleHelper::getLayoutPath('mod_helloworld_felder');
